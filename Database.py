import MySQLdb


def savetodatabase(examples_test,y,alldetails):

    insertsqlstatement = """INSERT INTO images(date,camera,congestion_probability,no_congestion_probability,finalClass,finalClassName) VALUES (%s,%s,%s,%s,%s,%s)"""
    stored_proc = """CALL persistance_check(%s)"""
    connection = MySQLdb.Connect(host="localhost",user="root",passwd="password",db="videos")
    x = connection.cursor()

    if len(examples_test) == y.shape[0]:
        for files,probs in zip(examples_test,y):
            data = files.split("/")
            camera = data[-2]
            datetime = data[-1].replace('.jpg', '')
            split = datetime.split("-")
            date = split[0] + ":" + split[1] + ":" + split[2] + " " + split[3] + ":" + split[4] + ":" + split[5]

            if int(probs[0]) > int(probs[1]):
                try:
                    x.execute(insertsqlstatement,(date,camera,probs[1],probs[0],0,"No Congestion"))
                    connection.commit()
                except Exception as e:
                    print e
                    connection.rollback()
            else:
                try:
                    x.execute(insertsqlstatement,(date,camera,probs[1],probs[0],1," Congestion"))
                    connection.commit()
                except Exception as e:
                    print e
                    connection.rollback()

        for [camera_name, camera_ip] in alldetails:
            # print str(camera_name)
	        x.callproc('persistance_check',[camera_name,])
