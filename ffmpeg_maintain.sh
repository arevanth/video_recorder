#!/bin/sh

check_process(){
	# check the args
    if [ "$1" = "" ];
    then
        return 0
    fi

    #PROCESS_NUM => get the process number regarding the given thread name
    PROCESS_NUM=$(ps -ef | grep "$1" | grep -v "grep" | wc -l)
    
    # for degbuging...
    $PROCESS_NUM
    
    if [ $PROCESS_NUM -eq 1 ];
    then
        return 1
    else
        return 0
    fi
}

while [ 1 ]; do
	#statements
	echo 'Checking...'
	check_process "python test_demo.py" 
	CHECK_RT = $?

	if [ $CHECK_RT -eq 0 ]; then
		#statements
	fi
	sleep 60
done