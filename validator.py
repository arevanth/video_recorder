import glob,os
import keras
import numpy as np
import Database
import fnmatch
from utils import utils
import sys

from scipy.misc import imread
from scipy.misc import imresize
from keras.models import model_from_json


def load_images():
    path1test = []
    image_path_1 = utils.path_to_images

    os.chdir(image_path_1)
    for root,dirnames,filenames in os.walk(image_path_1):
        for file in fnmatch.filter(filenames,'*.jpg'):
            path1test.append(os.path.join(root,file))

    return path1test


def examples_to_dataset(examples, block_size=2):
    X = []
    for path in examples:
        img = imread(path,mode='RGB')
        img_ht = int(800/3)
        img_wt = int(450/3)
        img = imresize(img,(img_ht,img_wt))
        X.append(img)
    return np.asarray(X)


def main():
    alldetails = utils.getallcamera()
    path1test = load_images()
    model_json = utils.model_json
    weights = utils.weights
    #Need to move the images to another folder.
    for [camera_name, camera_ip] in alldetails:
        new_path = '/home/camera/scanned_images/' + camera_name + '/'
        if not os.path.exists(new_path):
            os.mkdir(new_path)

    # load json and create model
    json_file = open(model_json, 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)

    # load weights into new model
    loaded_model.load_weights(weights)
    print("Loaded model from disk")

    while True:
        examples_test = [(path) for path in path1test]
        x_test = examples_to_dataset(examples_test)
        y = loaded_model.predict(x_test)

        Database.savetodatabase(examples_test, y, alldetails)

        print("Saved to the database.")

        #Need to move the images to another folder.
	    for path in path1test:
	        details = path.split("/")
	        camera = details[-2]
	        filename = details[-1]
	        os.rename(os.path.abspath(path), '/home/camera/scanned_images/' + camera + '/' + filename)

        #Load a fresh set of images.
        path1test = load_images()


if __name__ == "__main__":
    main()
