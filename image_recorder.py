import os
from utils import utils
__author__ = 'revanth'

def main():
    alldetails = utils.getallcamera()

    for [camera_name, camera_ip] in alldetails:
        directory = utils.path_to_images + camera_name + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)

        command = "ffmpeg -i " + camera_ip + " -vf fps=1/20 -strftime 1 " + directory  + '%Y-%m-%d-%H-%M-%S.jpg > ~/logs/' + camera_name +'.txt 2>&1 &'

        print(command)

        os.system(command)

if __name__ == "__main__":
    main()
