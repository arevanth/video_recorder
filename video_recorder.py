__author__ = 'revanth'

import os
import utils

def main():
    alldetails = utils.getallcamera()

    for [camera_name,camera_ip] in alldetails:
        directory = utils.path_to_videos + camera_name + '/'
        if not os.path.exists(directory):
            os.makedirs(directory)

        command = "ffmpeg -i " + camera_ip + " -c copy -t 00:15:00 -f segment -segment_list out.list -reset_timestamps 1 -segment_time 300 -segment_atclocktime 1 -strftime 1 -time_base 1/30 " + directory + '/' + '%m-%d-%y-%H-%M-%S.mkv </dev/null > /dev/null 2>&1 &'

        print(command)

        os.system(command)

if __name__ == "__main__":
    main()

