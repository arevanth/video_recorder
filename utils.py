#All the variables and method needed.
import os
from os.path import expanduser

class utils:

    list_of_camera = 'required_files/DesMoines.txt'
    base_path_to_media = expanduser("~") + "/Documents/Recording/"
    path_to_images = base_path_to_media + "images/"
    path_to_videos = base_path_to_media + "videos/"

    model_json = '/home/camera/video_recorder/required_files/model.json'
    weights = "/home/camera/video_recorder/required_files/model_weights_july22.h5"

    @classmethod
    def getallcamera(self):
        allcamdetails = [];
        dir = os.getcwd()
        filename = os.path.join(dir,utils.list_of_camera)

        with open(filename) as f:
            for line in f:
                details = line.split(",")
                details[1] = details[1].rstrip('\r\n')
                ipdetails = details[1].split('/')
                cameraname = ipdetails[-2].strip('lb.stream').upper()
                allcamdetails.append([cameraname, details[1]])

        return allcamdetails
