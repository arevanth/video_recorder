import sys
from PIL import Image
import pytesseract
import pyocr
import pyocr.builders
from scipy.misc import imresize

image = '/Users/revanth/Documents/recording/images/DMTV04/2017-10-03-21-04-46.jpg'
img = Image.open(image).convert('L')

cropped_img = img.crop((0,0,35,20))

cropped_img = imresize(cropped_img,(150,150))

cropped_img = Image.fromarray(cropped_img, 'L')

cropped_img.show()

tools = pyocr.get_available_tools()
if len(tools) == 0:
    print("No OCR tool found")
    sys.exit(1)

# The tools are returned in the recommended order of usage
tool = tools[0]
print("Will use tool '%s'" % (tool.get_name()))
# Ex: Will use tool 'libtesseract'

langs = tool.get_available_languages()
print("Available languages: %s" % ", ".join(langs))

lang = langs[0]
print("Will use lang '%s'" % (lang))

txt = tool.image_to_string(cropped_img,lang=lang,builder=pyocr.builders.TextBuilder())

print txt
print pytesseract.image_to_string(cropped_img)
